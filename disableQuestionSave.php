<?php
class disableQuestionSave extends PluginBase {

    static protected $name = 'disableQuestionSave';
    static protected $description = 'Proof plugin';
    
    
    public function init()
    {
        $this->subscribe('beforeQuestionSave');
    }

    public function beforeQuestionSave()
    {
        $this->getEvent()->get('model')->addError('title','Plugin disable save ');
    }
}
